# -*- coding: utf-8 -*-
class MockDBSessionForAdding(object):
    def __init__(self):
        self.added_objs = []
        self.idx = 0

    def add(self, obj):
        self.added_objs.append(obj)

    def commit(self):
        for obj in self.added_objs:
            if hasattr(obj, 'id') and obj.id != None:
                continue
            try:
                obj.id = self.idx
                self.idx += 1
            except AttributeError:
                pass


class MockDBModel(object):
    fields = []
    fields_preinit = {}

    def __init__(self):
        for key, init_fn in self.fields_preinit.items():
            setattr(self, key, init_fn())

    def __setattr__(self, key, value):
        if key not in self.fields:
            raise AttributeError("Incorrect field {}".format(key))
        self.__dict__[key] = value


def get_mock_db_model(name, fields, fields_preinit=None):
    fields_preinit = {} if fields_preinit is None else fields_preinit
    return type(name, (MockDBModel, ), {'fields': fields, 'fields_preinit': fields_preinit})