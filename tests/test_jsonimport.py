# -*- coding: utf-8 -*-
"""
This test module uses BaseImportTestCase, which defines all necessary tests.
"""

from mapper.jsonimport import JSONImporter
from .base_import_test_class import BaseImportTestClass


class TestJSONImport(BaseImportTestClass):
    def get_importer(self, db_session, fields_cfg):
        return JSONImporter(db_session, fields_cfg, 'items')

    def get_record_input_simple_test(self):
        input = """{
        "items": [
                {
                    "ident": "12",
                    "name": "I am a title",
                    "dt": "2017-06-10"
                }
            ]
        }"""
        return input

    def get_record_input_funny_symbols_test(self):
        input = """{
        "items": [
                {
                    "ident": "12",
                    "name": "I am a title",
                    "my-dt": "2017-06-10"
                }
            ]
        }"""
        return input
