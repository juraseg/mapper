# -*- coding: utf-8 -*-
"""
This class defines tests, which can be used with different importers.
To use them we need to define a subclass of `BaseImportTestCase` and implement functions:
- get_importer: should return instance of specific importer
- get_simple_test_record_input: this should return a string with test data for `simple` test
"""

from datetime import date

from mapper.schema import Schema, Field, Int, Str, Date


class MockDBSessionForAdding(object):
    def __init__(self):
        self.added_objs = []
        self.idx = 0

    def add(self, obj):
        self.added_objs.append(obj)

    def commit(self):
        for obj in self.added_objs:
            if hasattr(obj, 'id') and obj.id != None:
                continue
            try:
                obj.id = self.idx
                self.idx += 1
            except AttributeError:
                pass


class BaseImportTestClass(object):
    def get_importer(self, db_session, schema):
        raise NotImplementedError

    def get_record_input_simple_test(self):
        raise NotImplementedError

    def test_simple(self):
        class MockDBModel(object):
            def __setattr__(self, key, value):
                if key not in ['id', 'idx', 'title', 'date']:
                    raise AttributeError("Incorrect field {}".format(key))
                self.__dict__[key] = value

        db_session = MockDBSessionForAdding()
        input = self.get_record_input_simple_test()

        class MyCfg(Schema):
            __db_model__ = MockDBModel
            ident = Field('idx', Int())
            name = Field('title', Str())
            dt = Field('date', Date())
        importer = self.get_importer(db_session, MyCfg)
        importer.import_records(input)

        assert len(db_session.added_objs) == 1
        obj = db_session.added_objs[0]
        assert obj.idx == 12
        assert obj.title == 'I am a title'
        assert obj.date == date(2017, 6, 10)

    def get_record_input_funny_symbols_test(self):
        raise NotImplementedError

    def test_funny_symbols(self):
        class MockDBModel(object):
            def __setattr__(self, key, value):
                if key not in ['id', 'idx', 'title', 'date']:
                    raise AttributeError("Incorrect field {}".format(key))
                self.__dict__[key] = value

        db_session = MockDBSessionForAdding()
        input = self.get_record_input_funny_symbols_test()

        class MyCfg(Schema):
            __db_model__ = MockDBModel
            ident = Field('idx', Int())
            name = Field('title', Str())
            my_dt = Field('date', Date(), override_field_name='my-dt')
        importer = self.get_importer(db_session, MyCfg)
        importer.import_records(input)

        assert len(db_session.added_objs) == 1
        obj = db_session.added_objs[0]
        assert obj.idx == 12
        assert obj.title == 'I am a title'
        assert obj.date == date(2017, 6, 10)

