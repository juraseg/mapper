# -*- coding: utf-8 -*-
"""
This module tests Schema mapper with real database session (in-memory SQLite database).

It also shows off how models should be declared to work fine with Mapper
"""
from datetime import date

import pytest

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy import Table, Column, ForeignKey, UnicodeText, Integer, Date
from sqlalchemy.orm import relationship

from mapper.schema_mapper import import_records
from mapper import schema

# for purposes of this test module we use the same DB models for every test
# this is to avoid declaring too many db models
Base = declarative_base()


class Post(Base):
    __tablename__ = 'post'
    id = Column(Integer, primary_key=True)
    idx = Column(Integer, unique=True)
    title = Column(UnicodeText)
    date = Column(Date)
    # many-to-one
    author_id = Column(Integer, ForeignKey('author.id'), nullable=True)
    # one-to-one
    add_data = relationship('PostAddData', backref='post', uselist=False)
    # one-to-one with reversed declaration - child id in parent table
    add_data_reversed_id = Column(Integer, ForeignKey('post_add_data_reversed.id'))
    add_data_reversed = relationship('PostAddDataReversed', backref='post', uselist=False)
    # one-to-many
    comments = relationship('Comment', backref='post')
    # many-to-many
    # using back populates allows for bidirectional relationship
    tags = relationship(
        "Tag",
        secondary=lambda: post_tag_association_table,
        back_populates="posts")


class Author(Base):
    __tablename__ = 'author'
    id = Column(Integer, primary_key=True)
    name = Column(UnicodeText, unique=True)
    posts = relationship('Post', backref='author')


class PostAddData(Base):
    __tablename__ = 'post_add_data'
    post_id = Column(Integer, ForeignKey('post.id'), primary_key=True)
    text = Column(UnicodeText)


# to test that reversed declaration (with child.id in parent table) works too
class PostAddDataReversed(Base):
    __tablename__ = 'post_add_data_reversed'
    id = Column(Integer, primary_key=True)
    text = Column(UnicodeText)


class Comment(Base):
    __tablename__ = 'comment'
    id = Column(Integer, primary_key=True)
    title = Column(UnicodeText)
    author = Column(UnicodeText)
    date = Column(Date)
    text = Column(UnicodeText)
    post_id = Column(Integer, ForeignKey('post.id'))


# association table for many-to-many relationship of Posts and Tags
post_tag_association_table = Table('post_tag', Base.metadata,
    Column('post_id', Integer, ForeignKey('post.id')),
    Column('tag_id', Integer, ForeignKey('tag.id'))
)


class Tag(Base):
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True)
    name = Column(UnicodeText)
    posts = relationship(
        "Post",
        secondary=lambda: post_tag_association_table,
        back_populates="tags")


@pytest.fixture()
def session():
    """
    This function is called for every test in module.
    It connects to database and initializes schema.
    """
    # in-memory db
    db_uri = 'sqlite://'
    engine = create_engine(db_uri)
    session_factory = scoped_session(sessionmaker(bind=engine))

    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
    return session_factory()


def test_no_data(session):
    import_records(session, {}, [])


def test_simple(session):
    input = {'ident': u'12', 'name': u'I am a title', 'dt': u'2017-06-10'}

    class PostCfg(schema.Schema):
        __db_model__ = Post
        ident = schema.Field('idx', schema.Int())
        name = schema.Field('title', schema.Str())
        dt = schema.Field('date', schema.Date())

    import_records(session, PostCfg, [input])

    objs = session.query(Post).all()
    assert len(objs) == 1
    post_obj = objs[0]
    assert post_obj.idx == 12
    assert post_obj.title == 'I am a title'
    assert post_obj.date == date(2017, 6, 10)


def test_many_to_one_nested(session):
    input = {'ident': u'11',
             'author': {'name': u'Some dude'}}
    class AuthorCfg(schema.Schema):
        __db_model__ = Author
        name = schema.Field('name', schema.Str())
    class PostCfg(schema.Schema):
        __db_model__ = Post
        ident = schema.Field('idx', schema.Int())
        name = schema.Field('title', schema.Str())
        dt = schema.Field('date', schema.Date())
        author = schema.Relationship('author', AuthorCfg, uselist=False)

    import_records(session, PostCfg, [input])

    post_obj = session.query(Post).first()
    assert post_obj.idx == 11
    author_obj = post_obj.author
    assert author_obj.name == 'Some dude'


def test_many_to_one_flattened(session):
    input = {'ident': u'11',
             'author': u'Some dude'}
    class PostCfg(schema.Schema):
        __db_model__ = Post
        ident = schema.Field('idx', schema.Int())
        name = schema.Field('title', schema.Str())
        dt = schema.Field('date', schema.Date())
        author = schema.RelationshipFlattened('author', 'name', schema.Str(), Author, uselist=False)

    import_records(session, PostCfg, [input])

    post_obj = session.query(Post).first()
    assert post_obj.idx == 11
    author_obj = post_obj.author
    assert author_obj.name == 'Some dude'


def test_one_to_one_nested(session):
    input = {'ident': u'11',
             'add_data': {'text': u'Many data'}}
    class AddDataCfg(schema.Schema):
        __db_model__ = PostAddData
        text = schema.Field('text', schema.Str())
    class PostCfg(schema.Schema):
        __db_model__ = Post
        ident = schema.Field('idx', schema.Int())
        name = schema.Field('title', schema.Str())
        dt = schema.Field('date', schema.Date())
        add_data = schema.Relationship('add_data', AddDataCfg, uselist=False)

    import_records(session, PostCfg, [input])

    post_obj = session.query(Post).first()
    assert post_obj.idx == 11
    add_data_obj = post_obj.add_data
    assert add_data_obj.text == 'Many data'


def test_one_to_one_flattened(session):
    input = {'ident': u'11',
             'add_data': u'Many data'}
    class PostCfg(schema.Schema):
        __db_model__ = Post
        ident = schema.Field('idx', schema.Int())
        name = schema.Field('title', schema.Str())
        dt = schema.Field('date', schema.Date())
        add_data = schema.RelationshipFlattened('add_data', 'text', schema.Str(), PostAddData, uselist=False)

    import_records(session, PostCfg, [input])

    post_obj = session.query(Post).first()
    assert post_obj.idx == 11
    add_data_obj = post_obj.add_data
    assert add_data_obj.text == 'Many data'


def test_one_to_one_reversed_nested(session):
    input = {'ident': u'11',
             'add_data': {'text': u'Many data'}}
    class AddDataCfg(schema.Schema):
        __db_model__ = PostAddDataReversed
        text = schema.Field('text', schema.Str())
    class PostCfg(schema.Schema):
        __db_model__ = Post
        ident = schema.Field('idx', schema.Int())
        name = schema.Field('title', schema.Str())
        dt = schema.Field('date', schema.Date())
        add_data = schema.Relationship('add_data_reversed', AddDataCfg, uselist=False)

    import_records(session, PostCfg, [input])

    post_obj = session.query(Post).first()
    assert post_obj.idx == 11
    add_data_obj = post_obj.add_data_reversed
    assert add_data_obj.text == 'Many data'


def test_one_to_one_reversed_flattened(session):
    input = {'ident': u'11',
             'add_data': u'Many data'}
    class PostCfg(schema.Schema):
        __db_model__ = Post
        ident = schema.Field('idx', schema.Int())
        name = schema.Field('title', schema.Str())
        dt = schema.Field('date', schema.Date())
        add_data = schema.RelationshipFlattened('add_data_reversed', 'text', schema.Str(), PostAddDataReversed, uselist=False)

    import_records(session, PostCfg, [input])

    post_obj = session.query(Post).first()
    assert post_obj.idx == 11
    add_data_obj = post_obj.add_data_reversed
    assert add_data_obj.text == 'Many data'


def test_one_to_many_nested(session):
    input = {'ident': u'13',
             'comments': [{'title': u'Comment 1', 'author': u'A Guy', 'dt': u'2017-11-01', 'text': u'first one'},
                          {'title': u'Comment 2', 'author': u'Smart pants', 'dt': u'2017-11-02', 'text': u'again stupid comments from A Guy'}]}
    class CommentCfg(schema.Schema):
        __db_model__ = Comment
        title = schema.Field('title', schema.Str())
        author = schema.Field('author', schema.Str())
        dt = schema.Field('date', schema.Date())
        text = schema.Field('text', schema.Str())
    class PostCfg(schema.Schema):
        __db_model__ = Post
        ident = schema.Field('idx', schema.Int())
        name = schema.Field('title', schema.Str())
        dt = schema.Field('date', schema.Date())
        comments = schema.Relationship('comments', CommentCfg)

    import_records(session, PostCfg, [input])

    post_obj = session.query(Post).first()
    assert post_obj.idx == 13
    assert len(post_obj.comments) == 2
    for (comment_obj, comment_raw_data) in zip(post_obj.comments, input['comments']):
        assert comment_obj.title == comment_raw_data['title']
        assert comment_obj.author == comment_raw_data['author']
        assert comment_obj.text == comment_raw_data['text']


def test_many_to_many_flattened(session):
    input = {'ident': u'15',
             'tags': [u'a tag', u'the tag', u'tag-tag-tag']}
    class PostCfg(schema.Schema):
        __db_model__ = Post
        ident = schema.Field('idx', schema.Int())
        name = schema.Field('title', schema.Str())
        dt = schema.Field('date', schema.Date())
        tags = schema.RelationshipFlattened('tags', 'name', schema.Str(), Tag)

    import_records(session, PostCfg, [input])

    post_obj = session.query(Post).first()
    assert post_obj.idx == 15
    assert len(post_obj.tags) == 3
    for tag_obj in post_obj.tags:
        assert tag_obj.name in input['tags']
