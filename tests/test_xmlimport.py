# -*- coding: utf-8 -*-
"""
This test module uses BaseImportTestCase, which defines all necessary tests.
"""

from mapper.xmlimport import XMLImporter
from .base_import_test_class import BaseImportTestClass


class TestXMLImportFromChild(BaseImportTestClass):
    def get_importer(self, db_session, schema):
        return XMLImporter(db_session, schema, 'item', 'items')

    def get_record_input_simple_test(self):
        input = """<?xml version="1.0" encoding="UTF-8" ?>
<stuff>
    <items>
        <item>
            <ident>12</ident>
            <name>I am a title</name>
            <dt>2017-06-10</dt>
        </item>
    </items>
</stuff>"""
        return input

    def get_record_input_funny_symbols_test(self):
        input = """<?xml version="1.0" encoding="UTF-8" ?>
<stuff>
    <items>
        <item>
            <ident>12</ident>
            <name>I am a title</name>
            <my-dt>2017-06-10</my-dt>
        </item>
    </items>
</stuff>"""
        return input


class TestXMLImportFromRoot(BaseImportTestClass):
    def get_importer(self, db_session, schema):
        return XMLImporter(db_session, schema, 'item')

    def get_record_input_simple_test(self):
        input = """<?xml version="1.0" encoding="UTF-8" ?>
<stuff>
    <item>
        <ident>12</ident>
        <name>I am a title</name>
        <dt>2017-06-10</dt>
    </item>
</stuff>"""
        return input

    def get_record_input_funny_symbols_test(self):
        input = """<?xml version="1.0" encoding="UTF-8" ?>
<stuff>
    <item>
        <ident>12</ident>
        <name>I am a title</name>
        <my-dt>2017-06-10</my-dt>
    </item>
</stuff>"""
        return input
