# -*- coding: utf-8 -*-
from datetime import date

from mapper.dict_mapper import import_records
from .util import MockDBSessionForAdding, get_mock_db_model


def test_no_data():
    db_session = MockDBSessionForAdding()
    import_records(db_session, object, {}, [])
    assert len(db_session.added_objs) == 0


def test_simple():
    post_db_model = get_mock_db_model('Post', ['id', 'idx', 'title', 'date'])
    db_session = MockDBSessionForAdding()
    input = {'ident': u'12', 'name': u'I am a title', 'dt': u'2017-06-10'}
    fields_cfg = {'ident': ('idx', 'int'),
                  'name': ('title', 'str'),
                  'dt': ('date', 'iso-date')}
    import_records(db_session, post_db_model, fields_cfg, [input])

    assert len(db_session.added_objs) == 1
    obj = db_session.added_objs[0]
    assert obj.idx == 12
    assert obj.title == 'I am a title'
    assert obj.date == date(2017, 6, 10)


def test_relationship_not_list():
    post_db_model = get_mock_db_model('Post', ['id', 'idx', 'author'])
    author_db_model = get_mock_db_model('Author', ['id', 'name'])
    db_session = MockDBSessionForAdding()
    input = {'ident': u'11', 'name': u'Another title', 'dt': u'2017-11-01',
             'author': u'Some dude'}
    fields_cfg = {'ident': ('idx', 'int'),
                  'author': ('parent', ('author', author_db_model, 'name', 'str'))}
    import_records(db_session, post_db_model, fields_cfg, [input])

    main_obj = db_session.added_objs[-1]
    assert main_obj.idx == 11
    author_obj = main_obj.author
    assert author_obj.name == 'Some dude'


def test_relationship_nested_list():
    post_db_model = get_mock_db_model('Post', ['id', 'idx', 'comments'], {'comments': list})
    comment_db_model = get_mock_db_model('Comment', ['id', 'title', 'author', 'date', 'text', 'post_id'])
    db_session = MockDBSessionForAdding()
    input = {'ident': u'13',
             'comments': [{'title': u'Comment 1', 'author': u'A Guy', 'dt': u'2017-11-01', 'text': u'first one'},
                          {'title': u'Comment 2', 'author': u'Smart pants', 'dt': u'2017-11-02', 'text': u'again stupid comments from A Guy'}]}
    fields_cfg = {'ident': ('idx', 'int'),
                  'name': ('title', 'str'),
                  'dt': ('date', 'iso-date'),
                  'comments':
                      ('children',
                       ('comments', comment_db_model, {
                           'title': ('title', 'str'),
                           'author': ('author', 'str'),
                           'dt': ('date', 'iso-date'),
                           'text': ('text', 'str')})
                       )
                  }
    import_records(db_session, post_db_model, fields_cfg, [input])

    main_obj = db_session.added_objs[-1]
    assert main_obj.idx == 13
    assert len(main_obj.comments) == 2

    for (comment_obj, comment_raw_data) in zip(main_obj.comments, input['comments']):
        assert comment_obj.title == comment_raw_data['title']
        assert comment_obj.author == comment_raw_data['author']
        assert comment_obj.text == comment_raw_data['text']


def test_relationship_list_flattened():
    post_db_model = get_mock_db_model('Post', ['id', 'idx', 'tags'], {'tags': list})
    tag_db_model = get_mock_db_model('Comment', ['id', 'name'])
    db_session = MockDBSessionForAdding()
    input = {'ident': u'15',
             'tags': [u'a tag', u'the tag', u'tag-tag-tag']}
    fields_cfg = {'ident': ('idx', 'int'),
                  'tags': ('children_list', ('tags', tag_db_model, 'name', 'str'))
                  }
    import_records(db_session, post_db_model, fields_cfg, [input])

    main_obj = db_session.added_objs[-1]
    assert main_obj.idx == 15
    assert len(main_obj.tags) == 3
    for tag_obj in main_obj.tags:
        assert tag_obj.name in input['tags']
