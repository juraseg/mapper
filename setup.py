# -*- coding: utf-8 -*-
import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

requires = list(open('requirements.txt'))

test_requirements = list(open('requirements_test.txt'))

# TODO: think how to check for existence

setup(name='mapper',
      version='0.1',
      description='mapper',
      long_description=README,
      classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Topic :: Database",
        ],
      author='Yuri Abzyanov',
      author_email='yuri.abzyanov@gmail.com',
      packages=find_packages(),
      include_package_data=True,
      license='MIT',
      zip_safe=False,
      install_requires=requires,
      tests_require=test_requirements,
      )
