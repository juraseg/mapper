# Mapper

## Introduction

This library allows easily porting data from XML/JSON or other data format to database by means of SQLAlchemy.
The user needs to provide:
1) SQLAlchemy models for tables, to which the data will be added. It works with declarative meta
2) SQLAlchemy session, which uses the same engine as declarative meta, which defined models
3) Mapping "Schema". This is defined using classes in `mapper.schema` module.

When importing there is usually one main db table, other related tables are defined as `Relationship` to it.
The library can work with related tables of one-to-one, one-to-many, many-to-one and many-to-many types.
More on defining import schema in section "Import Schema".

## SQLAlchemy session and declarative base

Both session and declarative base should be bind to the same engine. 
So basically they should be created as for regular use.
For example:

```python
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class SomeModel(Base):
    ...
    
engine = create_engine('<db_uri>')
Base.metadata.bind(engine)
Session = sessionmaker(bind=engine)
session = Session()

```

## Models

The library works with declarative mapping. Classical mapping was not tested and probaby won't work.
For relationships to work properly the models should be defined with SQLAlchemy "relationship" properly 
to allow setting parent/child with simple assignment.

### Examples of models definitions of different relationship types

#### One-to-one

Here by parent we mean the main table.

```python
from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Parent(Base):
    ___tablename__ = 'parent'
    id = Column(Integer, primary_key=True)
    child = relationship('child', backref='parent', uselist=False)
    
class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))
```

Can also define with `child_id`:

```python
from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
class Parent(Base):
    ___tablename__ = 'parent'
    id = Column(Integer, primary_key=True)
    child_id = Column(Integer, ForeignKey('child.id'))
    child = relationship('child', backref='parent', uselist=False)
    
class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
```

#### One-to-many and Many-to-one

```python
from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)
    children = relationship('Child', backref='parent')

class Child(Base):
    ___tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))
```

The difference in these too is only seen when you import data (as one of the tables will be main).

#### Many-to-many

This one need association table.

```python
from sqlalchemy import Table, Column, ForeignKey, Integer
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
association_table = Table('left_right_assoc', Base.metadata,
    Column('left_id', Integer, ForeignKey('left.id')),
    Column('right_id', Integer, ForeignKey('right.id'))
)

class Left(Base):
    __tablename__ = 'left'
    id = Column(Integer, primary_key=True)
    children = relationship(
        "Right",
        secondary= association_table,
        back_populates="parents")

class Right(Base):
    ___tablename__ = 'right'
    id = Column(Integer, primary_key=True)
    parents = relationship(
        "Left",
        secondary=association_table,
        back_populates="children")
```

Using `back_populates` instead of `backref` allows defining relationship in both models,
however it's enough to have relationship in main model only to make import work fine.


## Schema

Import schema is similar to SQLAlchemy declarative or python formencode. 
Schema is class with attributes, which define how to import respective fields from input. 
It only imports fields, defining in Schema, and only if they exist in input row.
It's better explained on example:

```python
from mapper.schema import Schema, Field, Int, Str, Date

class SomeCfg(Schema):
    __db_model__ = MyAlchemyModel
    idx = Field('identifier', Int())
    name = Field('title', Str())
    dt = Field('date', Date())
```

In this case the mapper will create instance of `MyAlchemyModel`, then search for "idx" field in input row, 
transform it to integer (the `Int` class) and assign to `identifier` field of db model and assign 
to `identifier` field of created instance.
For simple fields usage is pretty straght-forward. Relationships are a bit more complex.

### Importing fields with special symbols

This scheme works fine, unless input field has symbols, which can't be used in python identifiers.
For example XML tag names are not constrained as much as python identifiers (they only don't allow < and >),
so they can be like "asd:qwe", "123:dfg".

In this case field name can be overridden by keyword `override_field_name`.
For example:

```python
from mapper.schema import Schema, Field, Int, Str, Date

class SomeCfg(Schema):
    __db_model__ = MyAlchemyModel
    idx = Field('identifier', Int())
    name = Field('title', Str())
    my_date = Field('date', Date(), override_field_name='my-date')
```

## Schema with relationships

First of all, relationships can be defined differently in input.

### Nested children

First case is when children are list of dicts.
For example: we have a "post" model, each post can have comments (one-to-many).
Input can define these comments as a list of rows, like this (JSON):

```json
{
    "posts": [
        {
            "title": "A post",
            "comments": [
                {
                    "text": "a comment"
                },
                {
                    "text": "another comment"
                }
            ]
        }
    ]
}
```

Here each comment can have several fields. What we do in this case is create schema for comments too, 
and then use `Relationship` constructor in schema for post:

```python
from mapper.schema import Schema, Field, Relationship, Str

class CommentCfg(Schema):
    __db_model__ = CommentModel
    text = Field(text, Str())

class PostCfg(Schema):
    __db_model__ = PostModel
    title = Field('title', Str())
    comments = Relationship('comments', CommentCfg)
```

### "Flattened" children

In this case children are just a list of values. This can work good for tags of our posts.

```json
{
    "posts": [
        {
            "title": "A post",
            "tags": ["a tag", "tag number 2" ]
        }
    ]
}
```

In this case we don't need to define separate schema for tags, will just define a RelationshipFlattened, 
which will take care of TagModel.

```python
from mapper.schema import Schema, Field, RelationshipFlattened, Str

class PostCfg(Schema):
    __db_model__ = PostModel
    title = Field('title', Str())
    tags = RelationshipFlattened('tags', 'name', Str(), TagModel)
```

To `RelationShipFlattened` constructor we passed following parameters:
- db model field - respective field name in main db model (in this case in PostModel)
- child model field - field name for the only value we have in our child model (in this case TagModel)
- value type
- child db model class


### One-to-one and Many-to-one

Sometimes the field in input row defines a value in related table, which is not a "child". 
We can have an "Author" table for posts author.
In this case we should pass `uselist=False` to relationship constructor. 
Works for both nested and flattened relationship.
Input row should be different in this case: it should not be using list. 
So it's either a nested dict or just a plain value.


## Importers

The library provides 2 simple importers out of the box: XMLImporter and JSONImporter.
It is easy to define new importers, just subclass mapper.baseimporter.BaseImporter 
and implement method `_iterator`. 
Can also override `__init__` method assuming that parent `__init__` is called in child `__init__`. 
Check out source code of base importers and XML and JSON importers for more details.

## Tests

To run tests execute in command line:

```bash
$ pytest
```
