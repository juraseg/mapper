# -*- coding: utf-8 -*-
from .schema_mapper import import_records as import_records_schema_cfg


class BaseImporter(object):
    """
    Base class for all importers.
    Subclasses must provide `_iterator` method.
    Also subclasses can override `__init__` method, assuming that they call parent `__init__`
    method in it. For example:
    ```
    class CSVImporter(BaseImporter):
        def __init__(self, db_session, fields_schema, dialect):
            super(CSVImporter, self).__init__(db_session, fields_schema)
            self.dialect = dialect
            ...
        def _iterator(self, s):
            reader = csv.DictReader(StringIO.StringIO(s), dialect=self.dialect)
            ...
    ```
    """
    def __init__(self, db_session, fields_schema):
        self.db_session = db_session
        self.fields_schema = fields_schema

    def _iterator(self, s):
        raise NotImplementedError

    def import_records(self, s):
        import_records_schema_cfg(self.db_session, self.fields_schema, self._iterator(s))
