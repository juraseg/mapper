# -*- coding: utf-8 -*-
from .schema import Field, Relationship, RelationshipFlattened


def import_records(db_session, schema, records_iter):
    for r in records_iter:
        db_rec = _process_rec(r, schema)
        db_session.add(db_rec)
        db_session.commit()


def _process_rec(row, schema):
    db_rec = _create_db_rec(row, schema)
    return db_rec


def _create_db_rec(row, schema):
    obj = schema.get_db_model()()
    for key, field_obj in schema.iterate_fields():
        input_field = field_obj.override_field_name if field_obj.override_field_name else key
        if input_field in row:
            val = row[input_field]
            assert isinstance(field_obj, Field) or \
                   isinstance(field_obj, Relationship) or \
                   isinstance(field_obj, RelationshipFlattened)
            if isinstance(field_obj, Relationship):
                _process_relationship(obj, field_obj, val)
            elif isinstance(field_obj, RelationshipFlattened):
                _process_relationshipd_flattened(obj, field_obj, val)
            elif isinstance(field_obj, Field):
                _process_field(obj, field_obj, val)
            else:
                raise AssertionError("Can't happen")
    return obj


def _process_relationship(obj, field_obj, val):
    if field_obj.uselist:
        for subrow in val:
            relation_obj = _create_db_rec(subrow, field_obj.relation_schema)
            getattr(obj, field_obj.db_field).append(relation_obj)
    else:
        relation_obj = _create_db_rec(val, field_obj.relation_schema)
        setattr(obj, field_obj.db_field, relation_obj)


def _process_relationshipd_flattened(obj, field_obj, val):
    relation_field_obj = field_obj.relation_field_obj
    if field_obj.uselist:
        for subval in val:
            relation_obj = field_obj.relation_db_model()
            _process_field(relation_obj, relation_field_obj, subval)
            getattr(obj, field_obj.db_field).append(relation_obj)
    else:
        relation_obj = field_obj.relation_db_model()
        _process_field(relation_obj, relation_field_obj, val)
        setattr(obj, field_obj.db_field, relation_obj)


def _process_field(obj, field_obj, val):
    assert isinstance(val, unicode)
    setattr(obj, field_obj.db_field, field_obj.type_processor.process(val))
