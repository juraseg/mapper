# -*- coding: utf-8 -*-
"""
This is an old implementation. Worked just as proof of concept. Use it on your own risk.
It's not supported by importers anymore.
"""
from decimal import Decimal
from datetime import datetime


import warnings
warnings.warn("\"dict\" mapper is not supported. "
              "Please use \"Schema\" mapper in `mapper.schema_mapper` module",
              Warning)


def import_records(db_session, mapped_model_cls, fields_cfg, records_iter):
    for r in records_iter:
        db_rec = _process_rec(r, fields_cfg, mapped_model_cls)
        db_session.add(db_rec)
        db_session.commit()


def _process_rec(row, fields_cfg, mapped_model_cls):
    db_rec = _create_db_rec(row, fields_cfg, mapped_model_cls)
    return db_rec


def _create_db_rec(row, fields_cfg, mapped_model_cls):
    obj = mapped_model_cls()
    for key, cfg in fields_cfg.items():
        if key in row:
            val = row[key]
            if cfg[0] == 'parent':
                _process_parent(obj, cfg, val)
            elif cfg[0] == 'children':
                _process_children(obj, cfg, val)
            elif cfg[0] == 'children_list':
                _process_children_list(obj, cfg, val)
            else:
                assert isinstance(val, unicode)
                db_key, val_type = cfg
                setattr(obj, db_key, _transform_value(val, val_type))
    return obj


def _process_parent(obj, cfg, val):
    db_key, parent_mapped_model_cls, parent_db_key, val_type = cfg[1]
    parent_obj = _create_db_rec({'field': val}, {'field': (parent_db_key, val_type)}, parent_mapped_model_cls)
    setattr(obj, db_key, parent_obj)


def _process_children(obj, cfg, val):
    db_key, child_model_cls, child_fields_cfg = cfg[1]
    for subrow in val:
        child_obj = _create_db_rec(subrow, child_fields_cfg, child_model_cls)
        getattr(obj, db_key).append(child_obj)


def _process_children_list(obj, cfg, val):
    db_key, child_model_cls, child_db_key, val_type = cfg[1]
    for subval in val:
        child_obj = _create_db_rec({'field': subval}, {'field': (child_db_key, val_type)}, child_model_cls)
        getattr(obj, db_key).append(child_obj)


# TODO: put allowed formats somewhere accessible for reference
# TODO: allow val_type as function
def _transform_value(raw_val, val_type):
    transformers = {
        'int': int,
        'decimal': Decimal,
        'float': float,
        'str': str,
        'utf-8': lambda s: s.decode('utf-8'),
        'iso-date': lambda s: datetime.strptime(s, '%Y-%m-%d').date(),
        'iso-datetime-utc': lambda s: datetime.strptime(s, "%Y-%m-%dT%H:%M:%S.%f"),
    }
    identity = lambda x: x
    return transformers.get(val_type, identity)(raw_val)
