# -*- coding: utf-8 -*-
import decimal
import datetime as dt

from .exceptions import SchemaConfigurationException

# 1) Schema class
# 2) field constructor
# 3) value type processors


class _BaseField(object):
    def __init__(self, db_field, override_field_name=None):
        self.db_field = db_field
        self.override_field_name = override_field_name


class Field(_BaseField):
    def __init__(self, db_field, type_processor, override_field_name=None):
        super(Field, self).__init__(db_field, override_field_name)
        self.type_processor = type_processor


class Relationship(_BaseField):
    def __init__(self, db_field, relation_schema, override_field_name=None, uselist=True):
        super(Relationship, self).__init__(db_field, override_field_name)
        self.relation_schema = relation_schema
        self.uselist = uselist


class RelationshipFlattened(_BaseField):
    def __init__(self, db_field, relation_db_field, relation_type_processor, relation_db_model,
                 override_field_name=None, uselist=True):
        super(RelationshipFlattened, self).__init__(db_field, override_field_name)
        self.relation_db_model = relation_db_model
        self.uselist = uselist
        self.relation_field_obj = Field(relation_db_field, relation_type_processor)


class TypeProcessorBase(object):
    def process(self, input_val):
        raise NotImplementedError


class Int(TypeProcessorBase):
    def process(self, input_val):
        return int(input_val)


class Decimal(TypeProcessorBase):
    def process(self, input_val):
        return decimal.Decimal(input_val)


class Float(TypeProcessorBase):
    def process(self, input_val):
        return float(input_val)


class Str(TypeProcessorBase):
    """
    This class represents "string" type in a sense of Python 3 - a unicode object.
    WARNING!!! NOT THE SAME as Python 2 "str".
    """
    def process(self, input_val):
        return input_val


class Date(TypeProcessorBase):
    def __init__(self, format='%Y-%m-%d'):
        self.format = format

    def process(self, input_val):
        return dt.datetime.strptime(input_val, self.format).date()


class Datetime(TypeProcessorBase):
    def __init__(self, format='%Y-%m-%dT%H:%M:%S.%f'):
        self.format = format

    def process(self, input_val):
        return dt.datetime.strptime(input_val, self.format)


class _SchemaMeta(type):
    def __init__(cls, name, bases, dict):
        is_parent = bases[0] == object
        if not is_parent and dict.get('__db_model__') is None:
            raise SchemaConfigurationException("`__db_model__` attribute is not defined!")
        super(_SchemaMeta, cls).__init__(name, bases, dict)
        cls._fields = []
        for k, v in dict.items():
            if isinstance(v, _BaseField):
                cls._fields.append(k)


class Schema(object):
    __metaclass__ = _SchemaMeta

    __db_model__ = None

    @classmethod
    def iterate_fields(cls):
        for k in cls._fields:
            yield k, getattr(cls, k)

    @classmethod
    def get_db_model(cls):
        return cls.__db_model__