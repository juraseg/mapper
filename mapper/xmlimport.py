# -*- coding: utf-8 -*-
import xmltodict

from .baseimporter import BaseImporter
from .exceptions import MapperInputException


class XMLImporter(BaseImporter):
    """
    Simple XML importer.
    It finds for "container_tag" in root structure, finds for "child_tag" items in it and processes
    them.

    It can be improved to allow arbitrary depth for "container_tag" and "child_tag",
    for example by using XPath.
    """
    def __init__(self, db_session, fields_schema, child_tag, container_tag=None):
        super(XMLImporter, self).__init__(db_session, fields_schema)
        self.child_tag = child_tag
        self.container_tag = container_tag

    def _iterator(self, s):
        data = xmltodict.parse(s)
        # have to get root key list this
        root_tag = data.keys()[0]
        children_root = data[root_tag]
        if self.container_tag:
            try:
                children_root = children_root[self.container_tag]
            except KeyError:
                raise MapperInputException("Container tag \"{}\" not found in XML".format(self.container_tag))
        if isinstance(children_root[self.child_tag], list):
            items = children_root[self.child_tag]
        else:
            # stupid workaround for case when there is only one child
            # in that case xmltodict treats it not as list
            items = [children_root[self.child_tag]]
        for item in items:
            yield item
