# -*- coding: utf-8 -*-
class MapperException(Exception):
    pass


class MapperConfigException(MapperException):
    pass


class MapperInputException(MapperException):
    pass


class SchemaConfigurationException(MapperException):
    pass