# -*- coding: utf-8 -*-
import json

from .baseimporter import BaseImporter
from .exceptions import MapperInputException


class JSONImporter(BaseImporter):
    """
    Simple JSONImporter.
    Finds "container_field" in root structure and processes its children (must be a JSON list).

    It can be improved to allow "container_field" to be in an arbitrary depth.
    """
    def __init__(self, db_session, fields_schema, container_field):
        super(JSONImporter, self).__init__(db_session, fields_schema)
        self.container_field = container_field

    def _iterator(self, s):
        data = json.loads(s)
        try:
            children_root = data[self.container_field]
        except IndexError:
            raise MapperInputException("Container field \"{}\" not found in JSON".format(self.container_field))
        for child in children_root:
            yield child
